import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

 import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { FormValidation } from '../pages/ui-component/form-validation/form-validation';
import { SegmentBar } from '../pages/ui-component/segment-bar/segment-bar';
import { ProductCard } from '../pages/ui-component/product-card/product-card';
//import { InAppBrowserPlugin } from '../pages/plugins/in-app-browser-plugin/in-app-browser-plugin';
// import { FileUpload } from '../pages/plugins/file-upload/file-upload'

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = ProductCard;
  pages: Array<{title: string, component: any}>;

  constructor(
    public platform: Platform, 
    public statusBar: StatusBar, 
    public splashScreen: SplashScreen) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'List', component: ListPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
     

      /*****************Use To set color to status bar********************** */
      this.statusBar.backgroundColorByHexString('#975906');
      
       /*****************Use To set Defualt to status bar********************** */
      this.statusBar.styleDefault();
      
      this.splashScreen.hide();

    });
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }
}


// http://bouncejs.com/

// Create Resources 
//ionic cordova resources