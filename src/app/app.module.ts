import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { DocumentViewer } from '@ionic-native/document-viewer';
import { FileOpener } from '@ionic-native/file-opener';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { FileChooser } from '@ionic-native/file-chooser';
//import { FilePath } from '@ionic-native/file-path'

/********************Provider***************************/
import { LoaderProvider } from '../providers/loader/loader';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { SwapableList } from '../pages/ui-component/swapable-list/swapable-list';
import { FormValidation } from '../pages/ui-component/form-validation/form-validation';
import { MultipleList } from '../pages/ui-component/multiple-list/multiple-list';
import { JsonDataServiceProvider } from '../providers/json-data-service/json-data-service';
import { HorizontalSegmentBar} from '../pages/ui-component/horizontal-segment-bar/horizontal-segment-bar';
import { SegmentBar } from '../pages/ui-component/segment-bar/segment-bar';
import { ProductCard } from '../pages/ui-component/product-card/product-card';
import { ShowMoreLess } from '../pages/ui-component/show-more-less/show-more-less'
import { InAppBrowserPlugin } from '../pages/plugins/in-app-browser-plugin/in-app-browser-plugin';
import { DocumentViewerPage } from '../pages/plugins/document-viewer-plugin/document-viewer';
import { FileOpenerPlugin } from '../pages/plugins/file-opener-plugin/file-opener-plugin';
import { FileTransferPlugin } from '../pages/plugins/file-transfer-plugin/file-transfer-plugin';
import { PluginsLocalNotificationPlugin } from '../pages/plugins/plugins-local-notification-plugin/plugins-local-notification-plugin';
import { AppMinimizePluginPage } from '../pages/plugins/app-minimize-plugin/app-minimize-plugin'
import { ExpandableAccordion } from '../pages/ui-component/expandable-accordion/expandable-accordion';
import { FileUpload } from '../pages/plugins/file-upload/file-upload';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    SwapableList,
    FormValidation,
    MultipleList,
    HorizontalSegmentBar,
    SegmentBar,
    ProductCard,
    ShowMoreLess,
    InAppBrowserPlugin,
    DocumentViewerPage,
    FileOpenerPlugin,
    FileTransferPlugin,
    PluginsLocalNotificationPlugin,
    AppMinimizePluginPage,
    ExpandableAccordion,
    FileUpload
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    SwapableList,
    FormValidation,
    MultipleList,
    HorizontalSegmentBar,
    SegmentBar,
    ProductCard,
    ShowMoreLess,
    InAppBrowserPlugin,
    DocumentViewerPage,
    FileOpenerPlugin,
    FileTransferPlugin,
    PluginsLocalNotificationPlugin,
    AppMinimizePluginPage,
    ExpandableAccordion,
    FileUpload
  ],
  providers: [
    StatusBar,
    SplashScreen,
    InAppBrowser,
    DocumentViewer,
    FileOpener,
    FileTransfer,
    File,
    LocalNotifications,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    JsonDataServiceProvider,
    LoaderProvider,
    FileChooser,
  //  FilePath
  ],
})
export class AppModule {}
