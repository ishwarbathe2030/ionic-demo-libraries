import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
/*
  Generated class for the JsonDataServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class JsonDataServiceProvider {

  constructor(public http: HttpClient) {
    console.log('Hello JsonDataServiceProvider Provider');
  }

  public getJSON(file): Observable<any> {
    return this.http.get("./assets/jsonfile/"+ file +".json");
}

}
