import { Component, ElementRef, ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { JsonDataServiceProvider } from '../../providers/json-data-service/json-data-service';


@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {

  public count: any = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public jsonDataService: JsonDataServiceProvider) {
  }


  ngOnInit() {
    this.statusCount()
  }

  public statusCount(): void {

    /*******************Get List for Select Option****************************** */
    this.jsonDataService.getJSON('statusCount').subscribe(data => {
      data.result.orderStatus.forEach(element => {
        if (element === 'Processing' ||  element === 'Delivered' ||  element === 'Failed') {
          this.count.push(data.result.statusCount[data.result.orderStatus.indexOf(element)])
        }
      });
      console.log(this.count)

    });
  }


}
