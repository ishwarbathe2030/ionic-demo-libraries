import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SwapableList } from '../ui-component/swapable-list/swapable-list';
import { FormValidation  } from '../ui-component/form-validation/form-validation'
import { MultipleList } from '../ui-component/multiple-list/multiple-list';
import { ProductCard } from '../ui-component/product-card/product-card';
import { ShowMoreLess } from '../ui-component/show-more-less/show-more-less';
import { InAppBrowserPlugin } from '../plugins/in-app-browser-plugin/in-app-browser-plugin';
import { DocumentViewerPage } from '../plugins/document-viewer-plugin/document-viewer';
import { FileOpenerPlugin } from '../plugins/file-opener-plugin/file-opener-plugin';
import { FileTransferPlugin } from '../plugins/file-transfer-plugin/file-transfer-plugin';
import { PluginsLocalNotificationPlugin } from '../plugins/plugins-local-notification-plugin/plugins-local-notification-plugin';
import { AppMinimizePluginPage } from '../plugins/app-minimize-plugin/app-minimize-plugin';
import { ExpandableAccordion } from '../ui-component/expandable-accordion/expandable-accordion'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  splash = true;

   public title:any = [
     {title:'Swapable List', page: SwapableList},
     {title:'Multiple Div', page: MultipleList},
     {title:'HTML Form Tag', page: FormValidation},
     {title:'Product Tab', page: ProductCard},
      {title:'Show more less', page: ShowMoreLess},
     {title:'In-app-Browser-Plugin', page: InAppBrowserPlugin},
     {title:'Document Viewer Plugin', page: DocumentViewerPage},
     {title:'File Opener Plugin', page: FileOpenerPlugin},
     {title:'File Transfer Plugin', page: FileTransferPlugin},
     {title:'Local Notification Plugin', page: PluginsLocalNotificationPlugin},
     {title:'App Minimize Plugin', page: ExpandableAccordion}
   ];
  constructor(public navCtrl: NavController) {
  }

  ioniViewDidLoad(){
    setTimeout(() => {
      this.splash = false;
    }, 4000 )
  }
  
  goToPage(page){
    this.navCtrl.push(page)
  }
   

}
