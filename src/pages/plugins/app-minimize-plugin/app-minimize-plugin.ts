import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Author : Ishwar Bathe
 * Date : 21 oct 2019
 * 
 * USE : Local notifications let an application inform its users that it 
 * has something for them. For example, a reminder or an new item for 
 * sale while the application isn’t running in the foreground. 
 * Local notifications are considered the best way by which an 
 * app can communicate with its user, even if the user is 
 * not actively using the app.
 * 
 * For More Info : https://www.sitepoint.com/integrating-local-notifications-in-cordova-apps/
 * 
 * https://github.com/katzer/cordova-plugin-local-notifications
 *
 * 1] ionic cordova plugin add cordova-plugin-local-notification
 * 2] npm install --save @ionic-native/local-notifications@4 
 * 
 * Diffrance Beetween Push and local notification 
 * Go to End of this file
 *
 */

@IonicPage()
@Component({
  selector: 'page-app-minimize-plugin',
  templateUrl: 'app-minimize-plugin.html',
})
export class AppMinimizePluginPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AppMinimizePluginPage');
  }

}
