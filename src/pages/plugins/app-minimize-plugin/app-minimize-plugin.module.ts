import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppMinimizePluginPage } from './app-minimize-plugin';

@NgModule({
  declarations: [
    AppMinimizePluginPage,
  ],
  imports: [
    IonicPageModule.forChild(AppMinimizePluginPage),
  ],
})
export class AppMinimizePluginPageModule {}
