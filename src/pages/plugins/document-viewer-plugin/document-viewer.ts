import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer'

/**
 * Author : Ishwar Bathe
 * 
 * Use following command to install plugin
 * 1]  cordova plugin add cordova-plugin-document-viewer
 * 2]  npm install --save @ionic-native/document-viewer@4
 * 
 *
 */

@IonicPage()
@Component({
  selector: 'document-viewer',
  templateUrl: 'document-viewer.html',
})
export class DocumentViewerPage {

  pdfSrc: string ;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private documentViewer:DocumentViewer,
    private file:File,
    private platform:Platform
    ) {
  }

  openPDF(url){
    const options: DocumentViewerOptions = {
      title: 'My PDF'
    }

    let path = null;
    if (this.platform.is('ios')) {
    path = this.file.documentsDirectory;
    } else if (this.platform.is('android')) {
    path = this.file.dataDirectory;
    }
    this.documentViewer.viewDocument(path +url, 'application/pdf', options)

  }


  openPdfByWndowOpen(url){
    window.open(url, '_blank', 'location=no');
  }


}
