import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { InAppBrowserPlugin } from './in-app-browser-plugin';

@NgModule({
  declarations: [
    InAppBrowserPlugin,
  ],
  imports: [
    IonicPageModule.forChild(InAppBrowserPlugin),
  ],
})
export class InAppBrowserPluginPageModule {}
