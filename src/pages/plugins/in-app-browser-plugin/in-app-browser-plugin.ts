import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { InAppBrowser,InAppBrowserOptions } from '@ionic-native/in-app-browser';


/**
 * Author : Ishwar Bathe
 * 
 * Step 1 : Use following command to install inapp-in-ionic-3
 * 
 * ionic cordova plugin add cordova-plugin-inappbrowser
 * npm install --save @ionic-native/in-app-browser@4
 * 
 * 
 * step2 : i]  Import Plugin in module.ts and
 *         ii] Add it into providers
 * 
 */


@IonicPage()
@Component({
  selector: 'in-app-browser-plugin',
  templateUrl: 'in-app-browser-plugin.html',
})
export class InAppBrowserPlugin {


  options : InAppBrowserOptions = {
    location : 'yes',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only ,shows browser zoom controls 
    hardwareback : 'yes',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'Close', //iOS only
    disallowoverscroll : 'no', //iOS only 
    toolbar : 'yes', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
    presentationstyle : 'pagesheet',//iOS only 
    fullscreen : 'yes',//Windows only    
};


  constructor(
    public navCtrl: NavController,
    private theInAppBrowser: InAppBrowser, 
    public navParams: NavParams) {
  }


  public openWithSystemBrowser(url : string){
    let target = "_system";
    this.theInAppBrowser.create(url,target,this.options);
}
public openWithInAppBrowser(url : string){
    let target = "_blank";
    this.theInAppBrowser.create(url,target,this.options);
}
public openWithCordovaBrowser(url : string){
    let target = "_self";
    this.theInAppBrowser.create(url,target,this.options);
} 


}
