import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FileUpload } from './file-upload';

@NgModule({
  declarations: [
    FileUpload,
  ],
  imports: [
    IonicPageModule.forChild(FileUpload),
  ],
})
export class FileUploadPageModule {}
