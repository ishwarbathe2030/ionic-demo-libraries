import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 *
 * 
 * 
 * File Chooser Plugin
 * 1] ionic cordova plugin add cordova-plugin-filechooser
 * 2] npm install --save @ionic-native/file-chooser@4
 * 
 * File Path Plugin
 * 1] ionic cordova plugin add cordova-plugin-filepath
 * 2] npm install --save @ionic-native/file-path@4
 * .
 */

@IonicPage()
@Component({
  selector: 'page-file-upload',
  templateUrl: 'file-upload.html',
})
export class FileUpload {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FileUploadPage');
  }

}
