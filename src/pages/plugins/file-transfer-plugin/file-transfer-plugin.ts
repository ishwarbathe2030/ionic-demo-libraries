import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer'


/**
 * Author : Ishwar Bathe
 * 
 * USE : This plugin allows you to upload and download files.
 *
 * 1] ionic cordova plugin add cordova-plugin-file-transfer
 * 2] npm install --save @ionic-native/file-transfer@4 
 * 
 * 
 * ionic cordova plugin add cordova-plugin-file
 * npm install --save @ionic-native/file@4
 */

@IonicPage()
@Component({
  selector: 'file-transfer-plugin',
  templateUrl: 'file-transfer-plugin.html',
})
export class FileTransferPlugin {


  

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private file:File,
    private transfer:FileTransfer,
    private platform:Platform,
    private docmuent:DocumentViewer
    ) {}

    openDownloadPDF(){

      let path = null;

      if(this.platform.is('ios')){
        path = this.file.documentsDirectory;
      }else{
        path = this.file.dataDirectory;
      }

      const transfer = this.transfer.create();
      transfer.download('http://188.95.36.103:8080/BuildersNetwork/show-file/barcode15456406300221570192904321.pdf/pdf',path+'myfile.pdf').then(entry =>{
        let url = entry.toURL();
        console.log(url)
        this.docmuent.viewDocument(url,'application/pdf',{});
      })
    }

}
