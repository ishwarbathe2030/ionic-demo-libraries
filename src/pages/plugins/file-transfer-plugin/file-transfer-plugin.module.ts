import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FileTransferPlugin } from './file-transfer-plugin';

@NgModule({
  declarations: [
    FileTransferPlugin,
  ],
  imports: [
    IonicPageModule.forChild(FileTransferPlugin),
  ],
})
export class FileTransferPluginPageModule {}
