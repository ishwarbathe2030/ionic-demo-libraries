import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FileOpenerPlugin } from './file-opener-plugin';

@NgModule({
  declarations: [
    FileOpenerPlugin,
  ],
  imports: [
    IonicPageModule.forChild(FileOpenerPlugin),
  ],
})
export class FileOpenerPluginPageModule {}
