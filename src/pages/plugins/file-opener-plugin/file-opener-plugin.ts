import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform } from 'ionic-angular';
import { FileOpener } from '@ionic-native/file-opener';
import { File } from '@ionic-native/file';

/**
 * Author : Ishwar Bathe
 * 
 * 
 * Purpose of this plugin
 *          : This plugin help to open a file in your device
 * 
 * 
 * Requirements :
 * 1] Cordova 7.0 or greter
 * 2] Android 4.4+ / iOS 9+ / WP8 / Windows
 * 3] This plugin requires the Android support library v4
 *   cordova plugin add cordova-plugin-file-opener2  --variable ANDROID_SUPPORT_V4_VERSION="27.+"
 *
 * Use Following command to install file opener plugin
 * 1] ionic cordova plugin add cordova-plugin-file-opener2
 * 2] npm install --save @ionic-native/file-opener@4
 * 
 */

@IonicPage()
@Component({
  selector: 'file-opener-plugin',
  templateUrl: 'file-opener-plugin.html',
})
export class FileOpenerPlugin {



  constructor(
    public navCtrl: NavController, 
    private platform:Platform,
    public navParams: NavParams,
    private fileOpener: FileOpener,
    private file:File
    ) {
  }

     
  openPDF(url){
   
    let path = null;
if (this.platform.is('ios')) {
path = this.file.documentsDirectory;
} else if (this.platform.is('android')) {
path = this.file.dataDirectory;
}
   

    this.fileOpener.open(path+url, 'application/pdf')
  .then(() => console.log('File is opened'))
  .catch(e => console.log('Error opening file', e));
  }

  openLOcalPdf(){

    let path = null;
if (this.platform.is('ios')) {
path = this.file.documentsDirectory;
} else if (this.platform.is('android')) {
path = this.file.dataDirectory;
}
    this.fileOpener.open(path+'assets/document/barcode15456406300221570192904321.pdf', 'application/pdf')
  .then(() => console.log('File is opened'))
  .catch(e => console.log('Error opening file', e));
  }

}
