import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MultiselectOptionsPage } from './multiselect-options';

@NgModule({
  declarations: [
    MultiselectOptionsPage,
  ],
  imports: [
    IonicPageModule.forChild(MultiselectOptionsPage),
  ],
})
export class MultiselectOptionsPageModule {}
