import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Author : Ishwar Bathe
 *
 * 
 * 
 */

@IonicPage()
@Component({
  selector: 'page-multiselect-options',
  templateUrl: 'multiselect-options.html',
})
export class MultiselectOptionsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MultiselectOptionsPage');
  }

}
