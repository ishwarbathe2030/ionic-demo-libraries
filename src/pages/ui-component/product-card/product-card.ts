import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Author : Ishwar Bathe
 * 
 * 
 */

@IonicPage()
@Component({
  selector: 'product-card',
  templateUrl: 'product-card.html',
})
export class ProductCard {

  
  productList:any = [
    {
      "productId": 1651,
      "productNo": "PR83956261",
      "name": "Cheese Slices",
      "defaultPrice": 300,
      "discountPercentage": null,
      "type": null,
      "defaultImage": "71549884149257.jpg",
      "inStock": true,
      "isActive": true,
      "avgRating": 0,
      "brand": "Britania",
      "quantity": null,
      "inActiveProduct": null,
      "activeProduct": null,
      "totalProduct": null,
      "mostSoldProdName": null,
      "priceDetails": {
        "productGroupId": 1835,
        "price": 300,
        "quantity": 1,
        "discount": 0,
        "discountedPrice": 300,
        "inStock": true
      },
      "attributeName": null,
      "attributeId": null,
      "productSold": 0,
      "discount": 0,
      "levels": "/Dairy/Cheese/Britania",
      "level1": null,
      "level2": null,
      "level3": null,
      "categoryId": 38,
      "productAttributes": [
        {
          "groupId": 1,
          "groupName": "Weight",
          "attributes": [
            {
              "productGroupId": 1835,
              "attributeId": 313,
              "attributeName": "476gm",
              "inStock": true,
              "price": 300
            }
          ],
          "productAttributes": [],
          "productNo": null
        }
      ]
    },
    {
      "productId": 195,
      "productNo": "PR12283977",
      "name": "Cheese Slices",
      "defaultPrice": 160,
      "discountPercentage": null,
      "type": null,
      "defaultImage": "Singlecheesslice.jpg",
      "inStock": false,
      "isActive": true,
      "avgRating": 0,
      "brand": "Kraft",
      "quantity": null,
      "inActiveProduct": null,
      "activeProduct": null,
      "totalProduct": null,
      "mostSoldProdName": null,
      "priceDetails": {
        "productGroupId": 217,
        "price": 160,
        "quantity": 0,
        "discount": 0,
        "discountedPrice": 160,
        "inStock": false
      },
      "attributeName": null,
      "attributeId": null,
      "productSold": 3,
      "discount": 0,
      "levels": "/Dairy/Cheese/Kraft",
      "level1": null,
      "level2": null,
      "level3": null,
      "categoryId": 39,
      "productAttributes": [
        {
          "groupId": 1,
          "groupName": "Weight",
          "attributes": [
            {
              "productGroupId": 217,
              "attributeId": 10,
              "attributeName": "200",
              "inStock": false,
              "price": 160
            }
          ],
          "productAttributes": [],
          "productNo": null
        }
      ]
    },
    {
      "productId": 197,
      "productNo": "PR59912413",
      "name": "Parmesan Cheese Grated",
      "defaultPrice": 399,
      "discountPercentage": null,
      "type": null,
      "defaultImage": "Kraft Parmesan Cheese Grated.jpeg",
      "inStock": true,
      "isActive": true,
      "avgRating": 0,
      "brand": "Kraft",
      "quantity": null,
      "inActiveProduct": null,
      "activeProduct": null,
      "totalProduct": null,
      "mostSoldProdName": null,
      "priceDetails": {
        "productGroupId": 219,
        "price": 399,
        "quantity": 1,
        "discount": 0,
        "discountedPrice": 399,
        "inStock": true
      },
      "attributeName": null,
      "attributeId": null,
      "productSold": 0,
      "discount": 0,
      "levels": "/Dairy/Cheese/Kraft",
      "level1": null,
      "level2": null,
      "level3": null,
      "categoryId": 39,
      "productAttributes": [
        {
          "groupId": 1,
          "groupName": "Weight",
          "attributes": [
            {
              "productGroupId": 219,
              "attributeId": 241,
              "attributeName": "85 gm",
              "inStock": true,
              "price": 399
            }
          ],
          "productAttributes": [],
          "productNo": null
        }
      ]
    },
    {
      "productId": 198,
      "productNo": "PR31796547",
      "name": "Procces  Cheddar Cheese",
      "defaultPrice": 265,
      "discountPercentage": null,
      "type": null,
      "defaultImage": "Kraft Procces  Cheddar Cheese.jpg",
      "inStock": true,
      "isActive": true,
      "avgRating": 0,
      "brand": "Kraft",
      "quantity": null,
      "inActiveProduct": null,
      "activeProduct": null,
      "totalProduct": null,
      "mostSoldProdName": null,
      "priceDetails": {
        "productGroupId": 220,
        "price": 265,
        "quantity": 1,
        "discount": 0,
        "discountedPrice": 265,
        "inStock": true
      },
      "attributeName": null,
      "attributeId": null,
      "productSold": 0,
      "discount": 0,
      "levels": "/Dairy/Cheese/Kraft",
      "level1": null,
      "level2": null,
      "level3": null,
      "categoryId": 39,
      "productAttributes": [
        {
          "groupId": 1,
          "groupName": "Weight",
          "attributes": [
            {
              "productGroupId": 220,
              "attributeId": 310,
              "attributeName": "190gm",
              "inStock": true,
              "price": 265
            }
          ],
          "productAttributes": [],
          "productNo": null
        }
      ]
    },
    {
      "productId": 200,
      "productNo": "PR34672826",
      "name": "Cheddar Cheese",
      "defaultPrice": 220,
      "discountPercentage": null,
      "type": null,
      "defaultImage": "CheddarCheese.jpg",
      "inStock": false,
      "isActive": true,
      "avgRating": 0,
      "brand": "Corona",
      "quantity": null,
      "inActiveProduct": null,
      "activeProduct": null,
      "totalProduct": null,
      "mostSoldProdName": null,
      "priceDetails": {
        "productGroupId": 223,
        "price": 220,
        "quantity": 0,
        "discount": 0,
        "discountedPrice": 220,
        "inStock": false
      },
      "attributeName": null,
      "attributeId": null,
      "productSold": 1,
      "discount": 0,
      "levels": "/Dairy/Cheese/Corona",
      "level1": null,
      "level2": null,
      "level3": null,
      "categoryId": 41,
      "productAttributes": [
        {
          "groupId": 1,
          "groupName": "Weight",
          "attributes": [
            {
              "productGroupId": 223,
              "attributeId": 10,
              "attributeName": "200",
              "inStock": false,
              "price": 220
            }
          ],
          "productAttributes": [],
          "productNo": null
        }
      ]
    },
    {
      "productId": 201,
      "productNo": "PR16534716",
      "name": "Parmesan Cheese",
      "defaultPrice": 300,
      "discountPercentage": null,
      "type": null,
      "defaultImage": "Coron Parmesan Cheese.jpg",
      "inStock": false,
      "isActive": true,
      "avgRating": 0,
      "brand": "Corona",
      "quantity": null,
      "inActiveProduct": null,
      "activeProduct": null,
      "totalProduct": null,
      "mostSoldProdName": null,
      "priceDetails": {
        "productGroupId": 224,
        "price": 300,
        "quantity": 0,
        "discount": 0,
        "discountedPrice": 300,
        "inStock": false
      },
      "attributeName": null,
      "attributeId": null,
      "productSold": 1,
      "discount": 0,
      "levels": "/Dairy/Cheese/Corona",
      "level1": null,
      "level2": null,
      "level3": null,
      "categoryId": 41,
      "productAttributes": [
        {
          "groupId": 1,
          "groupName": "Weight",
          "attributes": [
            {
              "productGroupId": 224,
              "attributeId": 10,
              "attributeName": "200",
              "inStock": false,
              "price": 300
            }
          ],
          "productAttributes": [],
          "productNo": null
        }
      ]
    },
    {
      "productId": 202,
      "productNo": "PR86376664",
      "name": "Gouda Cheese ",
      "defaultPrice": 275,
      "discountPercentage": null,
      "type": null,
      "defaultImage": "Corona Gouda Cheese.jpg",
      "inStock": false,
      "isActive": true,
      "avgRating": 0,
      "brand": "Corona",
      "quantity": null,
      "inActiveProduct": null,
      "activeProduct": null,
      "totalProduct": null,
      "mostSoldProdName": null,
      "priceDetails": {
        "productGroupId": 225,
        "price": 275,
        "quantity": 0,
        "discount": 0,
        "discountedPrice": 275,
        "inStock": false
      },
      "attributeName": null,
      "attributeId": null,
      "productSold": 0,
      "discount": 0,
      "levels": "/Dairy/Cheese/Corona",
      "level1": null,
      "level2": null,
      "level3": null,
      "categoryId": 41,
      "productAttributes": [
        {
          "groupId": 1,
          "groupName": "Weight",
          "attributes": [
            {
              "productGroupId": 225,
              "attributeId": 10,
              "attributeName": "200",
              "inStock": false,
              "price": 275
            }
          ],
          "productAttributes": [],
          "productNo": null
        }
      ]
    },
    {
      "productId": 203,
      "productNo": "PR99228233",
      "name": "Mozzerela Cheese",
      "defaultPrice": 200,
      "discountPercentage": null,
      "type": null,
      "defaultImage": "MozzerelaCheese1.jpg",
      "inStock": false,
      "isActive": true,
      "avgRating": 0,
      "brand": "Corona",
      "quantity": null,
      "inActiveProduct": null,
      "activeProduct": null,
      "totalProduct": null,
      "mostSoldProdName": null,
      "priceDetails": {
        "productGroupId": 226,
        "price": 200,
        "quantity": 0,
        "discount": 0,
        "discountedPrice": 200,
        "inStock": false
      },
      "attributeName": null,
      "attributeId": null,
      "productSold": 0,
      "discount": 0,
      "levels": "/Dairy/Cheese/Corona",
      "level1": null,
      "level2": null,
      "level3": null,
      "categoryId": 41,
      "productAttributes": [
        {
          "groupId": 1,
          "groupName": "Weight",
          "attributes": [
            {
              "productGroupId": 226,
              "attributeId": 10,
              "attributeName": "200",
              "inStock": false,
              "price": 200
            }
          ],
          "productAttributes": [],
          "productNo": null
        }
      ]
    },
    {
      "productId": 205,
      "productNo": "PR94746311",
      "name": "Process Cheese",
      "defaultPrice": 300,
      "discountPercentage": null,
      "type": null,
      "defaultImage": "Process Cheese.jpg",
      "inStock": false,
      "isActive": true,
      "avgRating": 0,
      "brand": "Corona",
      "quantity": null,
      "inActiveProduct": null,
      "activeProduct": null,
      "totalProduct": null,
      "mostSoldProdName": null,
      "priceDetails": {
        "productGroupId": 228,
        "price": 300,
        "quantity": 0,
        "discount": 0,
        "discountedPrice": 300,
        "inStock": false
      },
      "attributeName": null,
      "attributeId": null,
      "productSold": 0,
      "discount": 0,
      "levels": "/Dairy/Cheese/Corona",
      "level1": null,
      "level2": null,
      "level3": null,
      "categoryId": 41,
      "productAttributes": [
        {
          "groupId": 1,
          "groupName": "Weight",
          "attributes": [
            {
              "productGroupId": 228,
              "attributeId": 10,
              "attributeName": "200",
              "inStock": false,
              "price": 300
            }
          ],
          "productAttributes": [],
          "productNo": null
        }
      ]
    },
    {
      "productId": 1481,
      "productNo": "PR43276189",
      "name": "Ricotta Cheese",
      "defaultPrice": 190,
      "discountPercentage": null,
      "type": null,
      "defaultImage": "10 Ricotta Cheese1549354586543.jpg",
      "inStock": false,
      "isActive": true,
      "avgRating": 0,
      "brand": "Corona",
      "quantity": null,
      "inActiveProduct": null,
      "activeProduct": null,
      "totalProduct": null,
      "mostSoldProdName": null,
      "priceDetails": {
        "productGroupId": 1633,
        "price": 190,
        "quantity": 0,
        "discount": 0,
        "discountedPrice": 190,
        "inStock": false
      },
      "attributeName": null,
      "attributeId": null,
      "productSold": 0,
      "discount": 0,
      "levels": "/Dairy/Cheese/Corona",
      "level1": null,
      "level2": null,
      "level3": null,
      "categoryId": 41,
      "productAttributes": [
        {
          "groupId": 1,
          "groupName": "Weight",
          "attributes": [
            {
              "productGroupId": 1633,
              "attributeId": 9,
              "attributeName": "250",
              "inStock": false,
              "price": 190
            }
          ],
          "productAttributes": [],
          "productNo": null
        }
      ]
    },
    {
      "productId": 1650,
      "productNo": "PR65913612",
      "name": "Authentic - Sour Cream",
      "defaultPrice": 170,
      "discountPercentage": null,
      "type": null,
      "defaultImage": "5 sour1549883565694.jpg",
      "inStock": false,
      "isActive": true,
      "avgRating": 0,
      "brand": "Corona",
      "quantity": null,
      "inActiveProduct": null,
      "activeProduct": null,
      "totalProduct": null,
      "mostSoldProdName": null,
      "priceDetails": {
        "productGroupId": 1834,
        "price": 170,
        "quantity": 0,
        "discount": 0,
        "discountedPrice": 170,
        "inStock": false
      },
      "attributeName": null,
      "attributeId": null,
      "productSold": 0,
      "discount": 0,
      "levels": "/Dairy/Cheese/Corona",
      "level1": null,
      "level2": null,
      "level3": null,
      "categoryId": 41,
      "productAttributes": [
        {
          "groupId": 1,
          "groupName": "Weight",
          "attributes": [
            {
              "productGroupId": 1834,
              "attributeId": 305,
              "attributeName": "250gm",
              "inStock": false,
              "price": 170
            }
          ],
          "productAttributes": [],
          "productNo": null
        }
      ]
    },
    {
      "productId": 208,
      "productNo": "PR73474894",
      "name": "Goat Cheese ",
      "defaultPrice": 725,
      "discountPercentage": null,
      "type": null,
      "defaultImage": "Feta_Cheese.jpeg",
      "inStock": true,
      "isActive": true,
      "avgRating": 0,
      "brand": "Dodoni",
      "quantity": null,
      "inActiveProduct": null,
      "activeProduct": null,
      "totalProduct": null,
      "mostSoldProdName": null,
      "priceDetails": {
        "productGroupId": 231,
        "price": 725,
        "quantity": 1,
        "discount": 0,
        "discountedPrice": 725,
        "inStock": true
      },
      "attributeName": null,
      "attributeId": null,
      "productSold": 0,
      "discount": 0,
      "levels": "/Dairy/Cheese/Dodoni",
      "level1": null,
      "level2": null,
      "level3": null,
      "categoryId": 42,
      "productAttributes": [
        {
          "groupId": 1,
          "groupName": "Weight",
          "attributes": [
            {
              "productGroupId": 231,
              "attributeId": 10,
              "attributeName": "200",
              "inStock": true,
              "price": 725
            }
          ],
          "productAttributes": [],
          "productNo": null
        }
      ]
    }
  ]



  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProductCardPage');
  }


  changePrice(){

  }
  decreaseQuantity(){

  }

  increaseQuantity(){

  }
  addToCart(){
    
  }
}
