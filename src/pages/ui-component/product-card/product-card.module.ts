import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductCard } from './product-card';

@NgModule({
  declarations: [
    ProductCard,
  ],
  imports: [
    IonicPageModule.forChild(ProductCard),
  ],
})
export class ProductCardPageModule {}
