import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
/**
 * Author : Ishwar Bathe
 *
 */

@Component({
  selector: 'page-on-click-multiple-list',
  templateUrl: 'on-click-multiple-list.html',
})
export class OnClickMultipleListPage {

  public newDiv: any = [{}];
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  /**************This method is for simple div and and remove******************* */
  addNewDiv() {

    /***
     * When You click on add then simply one blank array push in array
     * That's why we creat one local array attr and then we push this into existing array
     * Ex Before Push newDiv array like newDiv=[];
     * when we click on add then we push attr={} into newDiv
     * After push newDiv array like newDiv=[{},{},{} so on ]
     * 
     * 
     * If you want by defualt one Div on screen 
     * For this you need to add one object array in newDiv
     * Ex : newDiv:any=[{}]
     * 
     * if u dont want to defualt div then simply 
     * Ex L newDiv:any=[]; 
     * */
    let attr = {};
    this.newDiv.push(attr);
  }
  removeAttribute(i) {
    /**
     * You need to pass index of object which you want to delete
     * 
     */
    this.newDiv.splice(i, 1);
  }





  /******************This method of Form element add remove************************* */

   

}
