import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Author : Ishwar Bathe
 * 
 * 
 */

@Component({
  selector: 'swapable-list',
  templateUrl: 'swapable-list.html',
})
export class SwapableList {

  ishwar:number=10
  segmentBarTitile: any = [
    { id: 1, name: "Shraddha" },
    { id: 2, name: "Anusha" },
    { id: 3, name: "Ishwar" },
    { id: 4, name: "Prasad" },
    { id: 5, name: "Vrushali" },
    { id: 4, name: "Amruta" },
    { id: 7, name: "Sachin" },
    { id: 6, name: "Prajakta" },
    { id: 9, name: "Bhumesh" },
    { id: 10, name: "Tejaswini" },
    { id: 11, name: "Tina" }
  ]


  constructor(public navCtrl: NavController, public navParams: NavParams) {

    var date = new Date();
    console.log(this.getNowDate(date))
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SwapableListPage');
  }



  getNowDate(date) {
    //return string
    var returnDate = "";
    //get datetime now
    //split
    var dd = date.getDate();
    var mm = date.getMonth() + 1; //because January is 0! 
    var yyyy = date.getFullYear();
    //Interpolation date
    if (dd < 10) {
        returnDate += `0${dd}.`;
    } else {
        returnDate += `${dd}.`;
    }

    if (mm < 10) {
        returnDate += `0${mm}.`;
    } else {
        returnDate += `${mm}.`;
    }
    returnDate += yyyy;
    return returnDate;
}

indexIconActive(a){
 console.log(a)
}


}
