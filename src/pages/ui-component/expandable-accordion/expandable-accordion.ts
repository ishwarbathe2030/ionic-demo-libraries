import { Component,Input} from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ExpandableAccordionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'expandable-accordion',
  templateUrl: 'expandable-accordion.html',
})
export class ExpandableAccordion {
   
  @Input() public accordionList:any = []

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ExpandableAccordionPage');
  }

}
