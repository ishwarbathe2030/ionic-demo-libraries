import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowMoreLess } from './show-more-less';

@NgModule({
  declarations: [
    ShowMoreLess,
  ],
  imports: [
    IonicPageModule.forChild(ShowMoreLess),
  ],
})
export class ShowMoreLessPageModule {}
