import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Author : Ishwar Bathe
 * Date : 10-10-2019
 *
 * 
 * 
 */

@IonicPage()
@Component({
  selector: 'show-more-less',
  templateUrl: 'show-more-less.html',
})
export class ShowMoreLess {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams) {
  }
 
}
