import { Component, ElementRef, ViewChild, Output, EventEmitter, Input } from '@angular/core';
import { NavController, NavParams, Platform, } from 'ionic-angular';

/**
 * Author : Ishwar Bathe
 *
 * 
 * 
 */

export interface TabModel {
  labelName?: string;
}



@Component({
  selector: 'horizontal-segment-bar',
  templateUrl: 'horizontal-segment-bar.html',
})
export class HorizontalSegmentBar {



  @ViewChild('slideElement') public slideElement: ElementRef;
  @ViewChild('sliderContainer') public sliderContainer: ElementRef;
  @ViewChild('sliderHead') public sliderHead: ElementRef;

  @Output('indexIconActive') public indexIconActive: EventEmitter<number> = new EventEmitter();
  @Output('scrollActive') public scrollActive: EventEmitter<boolean> = new EventEmitter();

  public indexSwipe: number = 0;
  public calc: number = 0;
  private eventPanDeltaX: number = 0;
  public scrollElement: number = 0;
  public iconOpacity: boolean;
  public blurActive: boolean;
  public labelPresent: boolean = true;
  public activeIndex: any;
  
  @Input('activeTabIndex') activeTabIndex: number;
  @Input('activeTabTextColor') activeTabTextColor: any;
  @Input('activeBorderBottomColor') activeBorderBottomColor: any;
  @Input('activeTabFontsize') activeTabFontsize: number;
  @Input('textColor') textColor: any;
   @Input('segmentList')  segmentBarTitile: any = [];


 
  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams) { }


  public ngOnInit(): void {
    if(this.activeTabIndex)
      this.activeIndex = this.activeTabIndex;
    else
      this.activeIndex = 0; 
  }



  public switchNext(index): void {
    this.activeIndex = index;
  }

  public onWindowScroll(): void {
    let element: any = this.sliderHead.nativeElement;
    if (element.offsetTop === 0) {
      if (element.offsetParent.offsetTop > this.scrollElement) {
        if (this.platform.is('ios')) {
          this.iconOpacity = false;
          this.blurActive = true;
        } else {
          this.iconOpacity = true;
          this.blurActive = false;
        }
        this.scrollActive.emit(true);
      } else {
        this.iconOpacity = false;
        this.blurActive = false;
        this.scrollActive.emit(false);
      }
    } else {
      if (element.offsetTop > this.scrollElement) {
        if (this.platform.is('ios')) {
          this.iconOpacity = false;
          this.blurActive = true;
        } else {
          this.iconOpacity = true;
          this.blurActive = false;
        }
        this.scrollActive.emit(true);
      } else {
        this.iconOpacity = false;
        this.blurActive = false;
        this.scrollActive.emit(false);
      }
    }
  }

  public lestener = (): void => {
    this.onWindowScroll();
  }

  public ngOnDestroy(): void {
    document.removeEventListener('scroll', this.lestener, true);
  }

  public ngAfterViewInit(): void {
    if (this.platform.is('ios') || this.platform.is('android')) {
      setTimeout(() => {
        if (this.sliderHead.nativeElement.offsetTop === 0) {
          this.scrollElement = this.sliderHead.nativeElement.offsetParent.offsetTop + 12;
        } else {
          this.scrollElement = this.sliderHead.nativeElement.offsetTop + 12;
        }
      }, 200);

      
     
    }
    console.log('width---' + this.sliderContainer.nativeElement.offsetWidth);
  }

  public swipe(index: number, emitIconActive?: boolean): void {
    let length;
    if (index >= 0 || index !== this.segmentBarTitile.length) {
      this.indexSwipe = index;
      this.animationSlide(this.indexSwipe, length, emitIconActive);
    }
  }

  public tapTransition(index: number, length: number, emitIconActive?: boolean): void {
    this.indexSwipe = index;
    this.animationSlide(index, length, emitIconActive);
  }

  private calcSwipeSlider(index?: number, length?: number): string {
    let lastElement = length - 2;
    if (index < lastElement) {
      if (index > 2) {
        this.calc = -this.slideElement.nativeElement.offsetWidth * index;
      } else {
        this.calc = 0;
      }
      if (index > 2) {
        this.calc += 50;
      }
      return this.calc + 'px';
    }
  }

  private animationSlide(index: number, length: number, emitIconActive?: boolean): void {
    this.sliderContainer.nativeElement.style.transform = 'translateX(' + this.calcSwipeSlider(index, length) + ')';
    this.sliderContainer.nativeElement.style.transition = 'transform 300ms';
    let width: number = -this.slideElement.nativeElement.offsetWidth;
    this.eventPanDeltaX = width * index;
    if (emitIconActive) {
      this.indexIconActive.emit(index);
    }
  }

  public move(event: any, eventName: string): void {
    let width: number = -this.slideElement.nativeElement.offsetWidth;
    this.sliderContainer.nativeElement.style.transition = 'transform 0ms';
    switch (eventName) {
      case 'left':
        if ((this.indexSwipe + 1) !== this.segmentBarTitile.length) {
          this.sliderContainer.nativeElement.style.transform = 'translateX(' + (this.eventPanDeltaX + event.deltaX) + 'px' + ')';
          if ((this.eventPanDeltaX + event.deltaX) <= (width * (this.indexSwipe + 1))) {
            this.indexSwipe++;
            // this.indexIconActive.emit(this.indexSwipe);
          }
        }
        break;
      case 'right':
        if (this.indexSwipe !== 0) {
          this.sliderContainer.nativeElement.style.transform = 'translateX(' + (this.eventPanDeltaX + event.deltaX) + 'px' + ')';
          if ((Math.abs(this.eventPanDeltaX) - event.deltaX) <= (Math.abs(width) * (this.indexSwipe - 1))) {
            this.indexSwipe--;
            // this.indexIconActive.emit(this.indexSwipe);
          }
        }
        break;
      case 'end':
        this.sliderContainer.nativeElement.style.transition = 'transform 300ms';
        this.sliderContainer.nativeElement.style.transform = 'translateX(' + (width * this.indexSwipe) + 'px' + ')';
        this.eventPanDeltaX = width * this.indexSwipe;
        // this.indexIconActive.emit(this.indexSwipe);
        break;
      default:
        break;
    }
  }



}
