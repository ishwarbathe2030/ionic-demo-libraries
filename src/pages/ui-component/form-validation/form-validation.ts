import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { JsonDataServiceProvider } from '../../../providers/json-data-service/json-data-service';

/**
 * Author : Ishwar Bathe
 * 
 * 
 */
//declare var $: any;
@Component({
  selector: 'form-validation',
  templateUrl: 'form-validation.html',
})


export class FormValidation {

  public friendList:any=[]

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public jsonDataService:JsonDataServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FormValidationPage');
  }
  testList: any = [
    {testID: 1, testName: " test1"},
    {testID: 2, testName: " test2"},
 ]

checkBoxData :any = [];
formData:any = [];

checkAll(){
for(let i =0; i <= this.testList.length; i++) {
 this.testList[i].checked = true;
}
console.log(this.testList);
}

ngOnInit(){


  /*******************Get List for Select Option****************************** */
  this.jsonDataService.getJSON('friendsList').subscribe(data => {
     this.friendList = data.result
     console.log(JSON.stringify(this.friendList))
   });


}



selectMember(data){
if (data.checked == true) {
 this.checkBoxData.push(data);
} else {
let newArray = this.checkBoxData.filter(function(el) {
  return el.testID !== data.testID;
});
this.checkBoxData = newArray;
}
this.formData.checkbox = this.checkBoxData;
console.log(this.formData);
}



}
