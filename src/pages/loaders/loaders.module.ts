import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LoadersPage } from './loaders';

@NgModule({
  declarations: [
    LoadersPage,
  ],
  imports: [
    IonicPageModule.forChild(LoadersPage),
  ],
})
export class LoadersPageModule {}
